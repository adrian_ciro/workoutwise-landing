// npm install -g gulp
// npm install gulp --save-dev
// npm install gulp-htmlmin gulp-if gulp-useref gulp-clean-css gulp-imagemin gulp-concat gulp-uglify gulp-minify-css gulp-usemin gulp-cache gulp-changed gulp-rename del --save-dev

var gulp = require('gulp'),
    htmlmin = require('gulp-htmlmin'),
    minifycss = require('gulp-minify-css'),
    useref = require('gulp-useref'),
    gulpif = require('gulp-if'),
    uglify = require('gulp-uglify'),
    imagemin = require('gulp-imagemin'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    cache = require('gulp-cache'),
    changed = require('gulp-changed'),
    del = require('del');

// Default task
gulp.task('default', ['useref'], function() {
    gulp.start('imagemin','copyfonts', 'copyothers');
});

// Clean
gulp.task('clean', function() {
    return del(['dist']);
});

gulp.task('useref', ['clean'], function () {
    gulp.src('./*.html')
        .pipe(useref())
        .pipe(gulpif('*.js', uglify()))
        .pipe(gulpif('*.css', minifycss()))
        .pipe(gulp.dest('dist/'));
});

gulp.task('imagemin', function() {
    gulp.src('./images/**/*')
        .pipe(gulp.dest('./dist/images'));
});

gulp.task('imagemin2', function() {
    return del(['dist/images/slider/swiper']), gulp.src('./images/slider/swiper/*')
        .pipe(cache(imagemin({ optimizationLevel: 4, progressive: true, interlaced: true })))
        .pipe(gulp.dest('dist/images/slider/swiper'));
});

gulp.task('copyfonts', function() {
    gulp.src('./css/fonts/**/*.{ttf,woff,eot,svg}*')
        .pipe(gulp.dest('./dist/css/fonts'));
    gulp.src('./css/et-line/**/*.{ttf,woff,eot,svg}*')
        .pipe(gulp.dest('./dist/css/et-line'));
});

gulp.task('copyothers', function() {
    gulp.src(['./.htaccess','./favicon.ico', './robots.txt'])
        .pipe(gulp.dest('./dist/'));
});

//Minify HTML/PHP -----------------> NEEDS TO RUN INDEPENDENTLY AFTER DEFAULT TASK
gulp.task('min', function() {
    gulp.src('./dist/*.html')
        .pipe(htmlmin({collapseWhitespace: true, minifyJS: true, removeComments: true}))
        .pipe(gulp.dest('dist'));
});